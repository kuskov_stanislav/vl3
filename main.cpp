#include <iostream>
#include <mutex>
#include <map>
#include <thread>

std::mutex _lock;

std::map<int, int> s_settings;
bool s_isCancel;

int GetKey() { return rand() % 100; }
int GetValue() { return rand(); }

void GenerateSettingsThread() {

    do {
        for (int i = 0; i < 15; ++i){
            _lock.lock();
            s_settings.erase(GetKey());
            _lock.unlock();
        }

        for (int i = 0; i < 40; ++i) {
            _lock.lock();
            s_settings[GetKey()] = GetValue();
            _lock.unlock();
        }
        std::this_thread::sleep_for(std::chrono::milliseconds((rand() % 1000) * 10));
    }
    while(!s_isCancel);
}

void RequestSettingsThread() {

    while (!s_isCancel) {

        _lock.lock();
        auto iter = s_settings.find(GetKey());
        _lock.unlock();

        if (iter != s_settings.end()){
            _lock.lock();
            std::cout << iter->second << std::endl;
            _lock.unlock();
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }

}

int main() {
    std::thread t1(&GenerateSettingsThread);
    std::thread t2(&RequestSettingsThread);

//    vector<thread> threads;
//    for(int i = 0; i < 10; ++i)
//        threads.push_back(thread(RequestSettingsThread));
//    for(auto &t: threads)
//        t.join();


    std::this_thread::sleep_for(std::chrono::milliseconds(30 * 1000));

    s_isCancel = true;

    t1.join();
    t2.join();
    std::cout << "main completed.\n";

    return 0;
}